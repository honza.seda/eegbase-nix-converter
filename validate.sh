#!/usr/bin/env bash

if [ $# -lt 2 ]
  then
    echo "Not enough arguments. Pass the search directory path as the first argument and the results file path as a second argument"
    exit
fi

dataset_path=$1
result_path=$2

echo "Running the nixio validation"
find "$dataset_path" -type f -name "*.nix" | while read line; do
  echo "Validating file $line"
  python -m nixio.cmd.validate "$line" >> "$result_path"
done
echo "Validation complete. The results are stored in $result_path"