"""
Converter configuration constants
"""

NIX_FILE_OUTPUT_DIR = "converted/"
NIX_FILE_EXTENSION = ".nix"
NIX_BLOCK_NAME = "EEG Data"

NIX_BLOCK_TYPE = "nix.session"
NIX_DATA_TYPE = "nix.data"
NIX_METADATA_TYPE = "nix.metadata"
NIX_ANNOTATION_TYPE = "nix.stimuli"

NIX_ODML_NAME = "Session metadata"
NIX_ODML_BASE_REPOSITORY = "https://portal.g-node.org/odml/terminologies/v1.1/terminologies.xml"

DEFAULT_ODML_MAPPING = "odml_mapping.json"
DEFAULT_BRAINVISION_MAPPING = "brainvision_mapping.json"
